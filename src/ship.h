#ifndef ship_h
#define ship_h

#include <Box2D/Box2D.h>
#include "sgame/sgame.h"
#include "sgame/types.h"


#define PI 3.1415

namespace sg {
	class View;
	class GameManager;
};

// Forward declarations
class ShipController;

class Ship: public sg::Entity {
	ENTITY_BOILERPLATE(Ship, "ship")

public:
	float get_size() const;

	sg::Vector2f get_position();

	void set_position(sg::Vector2f newPos);

	//! Set entity control
	void set_controller(ShipController* controller);

	void set_angle(float fi);

	float get_angle();

private:
	virtual void handle();

	float m_size;

	b2Body *m_body;

	ShipController* m_controller;
};


class ShipRepresentation: public sg::IEntityRepresentation {
public:
	ShipRepresentation(Ship& ship_entity);
	virtual ~ShipRepresentation();

	static sg::IEntityRepresentation* __stdcall create(sg::IEntity& entity);

	virtual void render();

private:
	Ship& m_entity;
	sg::TextureAsset* m_asset_texture;
};

class ShipController {
public:
	ShipController(sg::View& view) : m_view(view), m_controlled_ship(NULL) {}
	virtual ~ShipController() {}

	//! Set controlled entity
	void set_controlled_entity(Ship* ship) { m_controlled_ship = ship; }

	friend class Ship;
protected:
	//! calculate thrust vector
	virtual sg::Vector2f get_desired_thrust_vector() = 0;

	//! Calculate aim vector
	virtual sg::Vector2f get_desired_aim() = 0;

	//! Controller wants to fire
	virtual bool is_pulling_trigger() = 0;

	sg::View& m_view;
	Ship* m_controlled_ship;
};

class ShipPlayerController : public ShipController {
public:
	ShipPlayerController(sg::View& view);
	virtual ~ShipPlayerController();

	enum direction_t {up, down, left, right};

protected:
	sg::Vector2f get_desired_thrust_vector();
	sg::Vector2f get_desired_aim();
	bool is_pulling_trigger();

private:
	float m_scalar;
	float m_thrust[4];
};

#endif