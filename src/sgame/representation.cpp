#include <boost/bind.hpp>
#include <iostream>

#include "representation.h"

using namespace sg;
///////////////////////////////////////////////////////////////////////////////
// Summary:
//     Construct a new RepresentationManager
RepresentationManager::RepresentationManager(EntityManager& entityManager) :
		m_entityManager(entityManager) {

	// Connect entity signals
	m_entityManager.connect(
			boost::bind(&RepresentationManager::event_handler, this, _1));
}
///////////////////////////////////////////////////////////////////////////////
// Summary:
//     Destroy a RepresentationManager
RepresentationManager::~RepresentationManager() {

}

///////////////////////////////////////////////////////////////////////////////
// Summary:
//     returns singleton instance
// RepresentationManager* RepresentationManager::instance() {
// 	static RepresentationManager instance;
// 	return &instance;
// }

///////////////////////////////////////////////////////////////////////////////
// Summary:
//     Connected to signals
void RepresentationManager::event_handler(EntityEvent &event) {

	switch(event.event) {
	case ENTITY_CREATED:
		create_representation(event.get_entity_id());
		break;

	default:
		std::cout << "Some random event" << std::endl;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Summary:
//     Initialize singleton and connect event handler
void RepresentationManager::init() {
	
}

///////////////////////////////////////////////////////////////////////////////
// summary:
//     Register a new reprentation factory
//
// Parameters:
//     `entity_type_id`: std::string, Identification of entity type
//     `factory`: RepresentationFacory, function pointer to factory function
void RepresentationManager::register_representation(
		std::string entity_type_id, RepresentationFactory factory) {
	m_factories[entity_type_id] = factory;
}

///////////////////////////////////////////////////////////////////////////////
// summary:
//     Create a new representation instance for an entity, and add to
//     representation map. This is called when event handler recieves av 
//     ENTITY_CREATED event.
//
// Parameters:
//     `entity_id`: ID, id of entity to create representation for
void RepresentationManager::create_representation(ID entity_id) {
	IEntity &entity = m_entityManager.get_entity(entity_id); 
	RepresentationFactory factory = m_factories.at(entity.type_id());

	// Create representation for entity
	IEntityRepresentation* representation = (factory)(entity);

	m_representations.insert(entity_id, representation);
}

///////////////////////////////////////////////////////////////////////////////
// Summary:
//     Draw all representations to screen, using the specialised render function
//     on representation instances
void RepresentationManager::do_render() {
	RepresentationMap::iterator
		i = m_representations.begin(),
		e = m_representations.end();

	for(; i != e; i++) {
		i->second->do_render();
	}
}