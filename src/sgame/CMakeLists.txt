
set(
	SGAME_SOURCES

	entity.cpp
	representation.cpp
	types.cpp
	game.cpp
	view.cpp
	gameworld.cpp
	assets.cpp
	config.cpp
	camera.cpp
)

add_library (sgame ${SGAME_SOURCES})