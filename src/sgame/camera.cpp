#include "camera.h"
#include "types.h"
#include "entity.h"
#include "view.h"
#include "SDL/SDL_opengl.h"

using namespace sg;

///////////////////////////////////////////////////////////////////////////////
//
// Camera man
//
///////////////////////////////////////////////////////////////////////////////
// ----------------------------------------------------------------------------
CameraMan::CameraMan(View& view) : m_view(view), m_scale(20.0), m_position() {

}

// ----------------------------------------------------------------------------
CameraMan::~CameraMan() {

}

// ----------------------------------------------------------------------------
Vector2f CameraMan::position() {
	return m_position;
}

// ----------------------------------------------------------------------------
float CameraMan::scale() {
	return m_scale;
}

// ----------------------------------------------------------------------------
void CameraMan::zoomin(float amount) {
	Vector2f center = current_center();
	float factor = 1.0f + (amount / 10.0f);
	m_scale *= factor;
	center_on(center);
}

// ----------------------------------------------------------------------------
void CameraMan::zoomout(float amount) {
	Vector2f center = current_center();
	float factor = 1.0f + (amount / 10.0f);
	m_scale /= factor;
	center_on(center);
}

// ----------------------------------------------------------------------------
void CameraMan::render() {
	glTranslatef(m_position.x, m_position.y, 0.0f);
	glScalef(m_scale, m_scale, m_scale);
}

// ----------------------------------------------------------------------------
void CameraMan::center_on(Vector2f pos){
	m_position = (m_view.get_wh() / 2) - (pos * m_scale);
}

// ----------------------------------------------------------------------------
Vector2f CameraMan::current_center() {
	return m_view.get_wh() / (2 * m_scale) - m_position / m_scale;
}



///////////////////////////////////////////////////////////////////////////////
//
// Manual Camera man
//
///////////////////////////////////////////////////////////////////////////////
ManualCameraMan::ManualCameraMan(View& view) : CameraMan(view) {

}

// ----------------------------------------------------------------------------
ManualCameraMan::~ManualCameraMan() {

}
// ----------------------------------------------------------------------------
void ManualCameraMan::do_logic() {

}

// ----------------------------------------------------------------------------
void ManualCameraMan::panleft(float amount){
	m_position.x += amount;
}

// ----------------------------------------------------------------------------
void ManualCameraMan::panright(float amount){
	m_position.x -= amount;
}

// ----------------------------------------------------------------------------
void ManualCameraMan::panup(float amount){
	m_position.y += amount;
}

// ----------------------------------------------------------------------------
void ManualCameraMan::pandown(float amount){
	m_position.y -= amount;
}

///////////////////////////////////////////////////////////////////////////////
//
// Entity Camera man
//
///////////////////////////////////////////////////////////////////////////////
EntityCameraMan::EntityCameraMan(View& view)
: CameraMan(view), m_entity(NULL) {

}

// ----------------------------------------------------------------------------
EntityCameraMan::~EntityCameraMan() {

}

// ----------------------------------------------------------------------------
void EntityCameraMan::follow_entity(Entity* entity) {
	m_entity = entity;
}

// ----------------------------------------------------------------------------
void EntityCameraMan::do_logic() {
	// Simple P regulator
	if(!m_entity)
		return;

	Vector2f dpdt = m_entity->get_position() - m_old_position;
	Vector2f goal;

	goal = m_entity->get_position(); // + dpdt * 50;

	Vector2f diff = goal - this->current_center();
	Vector2f v = diff * 0.05;
	this->center_on(this->current_center() + v);
	m_old_position = m_entity->get_position();
}