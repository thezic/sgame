#include "config.h"
#include "exception.h"
#include "../pugixml/pugixml.hpp"
#include <iostream>

using namespace sg;



// -------------------------------------------------------------------
Config::Config() {
	// Default config
	//m_properties["assets_definition_xml"] = "assets.xml";
	//m_properties["resource_dir"] = "/Users/simon/Dev/game/sgame/resources/";
}

// -------------------------------------------------------------------
Config::~Config() {

}

// -------------------------------------------------------------------
std::string Config::get_string(std::string property) {
	return m_properties[property];
}

// -------------------------------------------------------------------
Config& Config::instance() {
	static Config config;
	return config;
}

// -------------------------------------------------------------------
bool Config::load_config_file(const char* path, bool no_exception) {
	std::cout << "Loading config from file: \"" << path << "\"" << std::endl;

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(path);

	if (!result) {
		if (no_exception) { return false; }
		throw XmlFileError(result);
	}

	pugi::xpath_node_set config_set = doc.select_nodes("/config//data");

	pugi::xpath_node_set::const_iterator it;
	for (it = config_set.begin(); it != config_set.end(); ++it) {
		pugi::xpath_node node = *it;

		std::string
				name = node.node().attribute("name").value(),
				value = node.node().attribute("value").value();

		// Add setting
		m_properties[name] = value;
		std::cout << name << ": " << value << std::endl;
	}
	return true;
}