#ifndef events_h
#define events_h

#include "types.h"

namespace sg {
	enum EVENT {
		ENTITY_CREATED
	};

	class Event {
	public:
		Event(EVENT event, LPARAM data=0) : event(event), data(data){}
		virtual ~Event() {}
		const EVENT event;
		const LPARAM data;
	};

	class EntityEvent : public Event {
	public:
		EntityEvent(EVENT event_id, ID entity_id)
			: Event(event_id, entity_id), entity_id(entity_id) {}
		virtual ~EntityEvent() {}

		ID get_entity_id() const { return entity_id; }

	private:
		const ID entity_id;
	};
};

#endif