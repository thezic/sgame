#ifndef gameworld_h
#define gameworld_h

#include <string>
#include <boost/ptr_container/ptr_vector.hpp>
#include <Box2D/Box2D.h>

class Tile;

namespace sg {
	class TextureAsset;
	class GameManager;
};

class ContactListener : public b2ContactListener {
	void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
};

class GameWorld {
public:
	GameWorld(sg::GameManager& owner);
	~GameWorld();

	void pre_camerman_render();
	void render_world();
	void load_level(std::string level);

	b2World* world() { return m_world; }

	void do_logic(double timestep);

private:
	float m_width;
	float m_height;
	boost::ptr_vector<Tile> m_tiles;
	sg::TextureAsset* m_asset_background;
	sg::GameManager& m_gm;
	b2World* m_world;
	ContactListener m_contact_listener;
};

#endif