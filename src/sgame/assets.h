#ifndef ASSETS_H
#define ASSETS_H

#include <string>
#include <boost/function.hpp>
#include <boost/ptr_container/ptr_map.hpp>
#include "../pugixml/pugixml.hpp"
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"

#define GAM GameAssetManager::instance()

namespace sg {

	class IGameAsset;

	typedef boost::function<IGameAsset* (pugi::xml_node)> AssetFactory;

	typedef boost::ptr_map<std::string, IGameAsset> AssetMap;

	// -----------------------------------------------------------------
	//! Game asset manager
	/*!
	    Manages all sorts of game assets, such as graphics, sounds, etc
	    All assets are defined in the xml-file defined by app config
	*/
	class GameAssetManager {
	public:
		GameAssetManager();
		virtual ~GameAssetManager();

		//! Register asset factory
		/*!
		    Associates a IGameAsset derivative with an identifier to
		    create assets.

		    \param id string asset type id
		    \param factory function pointer to create assets
		*/
		void register_asset_type(std::string id, AssetFactory factory);

		//! Fetch an asset from memory.
		/*! 
		  Takes the name of an asset and returns corresponding asset.
		  If no corresponding asset is found, tries to load it. If it can't
		  be loaded an exception will be raised.

		  \param asset_name name of asset
		  \return reference to asset
		*/
		IGameAsset& get_asset(std::string asset_name);

		//! Load an asset to memory
		/*!
		    Load an asset identied by name in asset xml file.

		    \param asset_name name of asset in xml file
		*/
		void load_asset(std::string asset_name);

		//! Load asset xml file to memory
		/*!
			If unable to load file, throws XmlFileError
		*/
	    void load_file();

		//! return instance of manager
		static GameAssetManager& instance();

	private:
		//! Load asset from xml node
		/*!
		    \param node xml node to load asset from
		*/
	    void load_asset_from_node(pugi::xml_node node);

	private:
		bool m_xml_is_loaded;
		pugi::xml_document m_doc;
		AssetMap m_assets;
		std::map<std::string, AssetFactory> m_factories;

	};

	class IGameAsset {
	public:
		IGameAsset() {};
		virtual ~IGameAsset() {};
	};

	class TextureAsset : public IGameAsset {
	public:
		TextureAsset(pugi::xml_node node);
		virtual ~TextureAsset();

		void hello() {
			std::cout << "Hello, im a texture asset" << std::endl;
		};

		GLuint texture();
		float aspect_ratio();

		static IGameAsset* __stdcall create(pugi::xml_node node);

	private:
		GLuint m_texture;
		GLenum m_texture_format;
		int m_width;
		int m_height;
		int m_bpp;
	};
};

#endif // ASSETS_H