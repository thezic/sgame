#ifndef sgame_representation_h
#define sgame_representation_h
#include <map>
#include <boost/ptr_container/ptr_map.hpp>
#include "entity.h"

#define ENTITY_REPRESENTATION_BOILERPLATE(ReprKlass, EntKlass) \
public: \
	ReprKlass(EntKlass& entity) : m_entity(entity) { initialize_entity(); } \
	virtual ~ReprKlass() { destroy_entity(); } \
	static sg::IEntityRepresentation* __stdcall create(sg::IEntity& entity) { \
		return new BulletRepresentation(static_cast<EntKlass&>(entity)); \
	} \
private: \
		EntKlass& m_entity;

namespace sg {

	class IEntityRepresentation;

	typedef boost::function<IEntityRepresentation* (IEntity&)> RepresentationFactory;
	typedef boost::ptr_map<ID, IEntityRepresentation> RepresentationMap;

	////////////////////////////////////////////////////////////////////////////
	// Summary:
	//    Singleton for managing all entity representation instances.
	class RepresentationManager {
	public:
		RepresentationManager(EntityManager& entityManager);
		~RepresentationManager();

		//static RepresentationManager* instance();

		void init();
		void event_handler(EntityEvent &event);
		void register_representation(std::string entity_type_id, RepresentationFactory factory);
		void do_render();

	private:
		void create_representation(ID entity_id);

	private:
		EntityManager& m_entityManager;
		boost::signals2::connection m_connection;

		std::map<std::string, RepresentationFactory> m_factories;
		RepresentationMap m_representations;
	};

	////////////////////////////////////////////////////////////////////////////
	// Summary:
	//    Interface that describes an entity representation.
	class IEntityRepresentation {
	public:
		IEntityRepresentation() {};
		virtual ~IEntityRepresentation() {};

		virtual void do_render() {
			render();
		}
		
	private:
		virtual void render() = 0;
		
	};
};

#endif