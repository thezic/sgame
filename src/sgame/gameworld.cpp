#include "gameworld.h"
#include "types.h"
#include "config.h"
#include "assets.h"
#include "game.h"
#include "../ship.h"
#include "../pugixml/pugixml.hpp"
#include <Box2D/Box2D.h>
#include <SDL/SDL_opengl.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <map>

#define foreach BOOST_FOREACH

// ----------------------------------------------------------------------------
std::string get_resource_dir() {
	return sg::Config::instance().get_string("resource_dir");
}

// ----------------------------------------------------------------------------
class Tile {
public:
	enum tile_type { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, BLOCK };
	friend std::ostream& operator<< (std::ostream&, const Tile&);
	
	Tile(tile_type type, float x, float y):
		type(type), x(x), y(y) { }
	virtual ~Tile() { }

	void render() {
		glTranslatef(this->x, this->y, 0.0f);
		glBegin(GL_LINE_LOOP);
			glColor4f(0.f, 0.f, 1.f, 1.f);
			switch(type) {
			case TOP_LEFT:
				glVertex2f(-0.5f, -0.5f);
				glVertex2f( 0.5f,  0.5f);
				glVertex2f( 0.5f, -0.5f);
				break;

			case TOP_RIGHT:
				glVertex2f(-0.5f,  0.5f);
				glVertex2f( 0.5f, -0.5f);
				glVertex2f(-0.5f, -0.5f);
				break;

			case BOTTOM_LEFT:
				glVertex2f(-0.5f,  0.5f);
				glVertex2f( 0.5f,  0.5f);
				glVertex2f( 0.5f, -0.5f);
				break;

			case BOTTOM_RIGHT:
				glVertex2f(-0.5f,  0.5f);
				glVertex2f( 0.5f,  0.5f);
				glVertex2f(-0.5f, -0.5f);
				break;

			default:
				glVertex2f(-0.5f, -0.5f);
				glVertex2f( 0.5f, -0.5f);
				glVertex2f( 0.5f,  0.5f);
				glVertex2f(-0.5f,  0.5f);
			}
		glEnd();
	}
private:
	tile_type type;
	float x;
	float y;
};

// ----------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& stream, const Tile& tile) {
	switch(tile.type) {
	case Tile::TOP_LEFT:
		stream << "TL, ";
		break;

	case Tile::TOP_RIGHT:
		stream << "TR, ";
		break;

	case Tile::BOTTOM_LEFT:
		stream << "BL, ";
		break;

	case Tile::BOTTOM_RIGHT:
		stream << "BR, ";
		break;

	case Tile::BLOCK:
		stream << "##, ";
		break;
	}
	return stream << "(" << tile.x << ", " << tile.y << ")";
}

// ----------------------------------------------------------------------------
class Tileset {
public:
	Tileset(std::string name);
	virtual ~Tileset();

private:
	void load_tileset();

	struct Rect {
		int x;
		int y;
		int w;
		int h;
	};

private:
	std::string m_descrFilename;
	std::string m_tilesetFilename;

	std::map<sg::ID, Rect> m_tileRects;
};


// ----------------------------------------------------------------------------
Tileset::Tileset(std::string name) {
	m_descrFilename = name + ".json";
	load_tileset();
}

// ----------------------------------------------------------------------------
Tileset::~Tileset(){

}

// ----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
// Summary:
//    load tileset from file
void Tileset::load_tileset(){
	std::cout << "tileset: " << m_descrFilename << std::endl;
	using boost::property_tree::ptree;
	ptree pt;

	std::string filename = get_resource_dir() + "tilesets/" + m_descrFilename;

	read_json(filename, pt);

	m_tilesetFilename = pt.get<std::string>("filename");
	std::cout << "tileset data: " << m_tilesetFilename << std::endl;

	// extract tile information
	BOOST_FOREACH(ptree::value_type &v, pt.get_child("tiles")) {
		ptree &node = v.second;
		Rect r;
		int id = node.get<int>("id");
		r.x = node.get<int>("x");
		r.y = node.get<int>("y");
		r.w = node.get<int>("w");
		r.h = node.get<int>("h");

		m_tileRects[id] = r;
	}
}

////////////////////////////////////////////////////////////////////////////////
//
// GameWorld
//
////////////////////////////////////////////////////////////////////////////////
GameWorld::GameWorld(sg::GameManager& owner) : m_gm(owner) {
	m_width = 40.0;
	m_height = 30.0f;
	// Create static things

	// Ground box

	// Creaate static world
    /*b2BodyDef groundDef;

    groundDef.position.Set(5.f, 5.f);
    b2Body *ground = m_world->CreateBody(&groundDef);

    b2Vec2 vs[4];
    vs[0].Set(0.0f, 1.0f);
    vs[1].Set(3.0f, 0.0f);
    vs[2].Set(7.0f, 0.0f);
    vs[3].Set(10.0f, 1.0f);


    b2ChainShape groundChain;
    groundChain.CreateChain(vs, 4);*/

    /*b2Body *ground2 = m_world->CreateBody(&groundDef)
    b2Vec2 vs[4];
    vs[0].Set(5.0f, 3.0f);
    vs[1].Set(7.0f, 4.0f);
    vs[2].Set(12.0f, 4.0f);
    vs[3].Set(14.0f, 3.0f);

    b2ChainShape groundChain2.CreateChain(vs, 4);*/



   //ground->CreateFixture(&groundChain, 0.0f);

    // Initialize world
	b2Vec2 gravity(0.0f, -10.0f);
	m_world = new b2World(gravity);
	m_world->SetContactListener(&m_contact_listener);
}

// ----------------------------------------------------------------------------
GameWorld::~GameWorld() {
	delete m_world;
}

// ----------------------------------------------------------------------------
void GameWorld::load_level(std::string level) {
	std::cout << "Loading level: " << level << std::endl;

	std::string filename = get_resource_dir() + "maps/" + level + ".xml";
	std::cout << filename << std::endl;

	pugi::xml_document doc;
	pugi::xml_node meta_node;
	pugi::xml_parse_result result = doc.load_file(filename.c_str());

	std::cout << "Load result: " << result.description() << std::endl;
	if(!result) {
		return;
	}

	int width, height;

	meta_node = doc.child("map").child("meta");
	width = atoi(
		meta_node.find_child_by_attribute("data", "name", "width")
		.attribute("value")
		.value());

	height = atoi(
		meta_node.find_child_by_attribute("data", "name", "height")
		.attribute("value")
		.value());

	std::cout << "name: " << 
		meta_node.find_child_by_attribute("data", "name", "name")
			.attribute("value")
			.value() << std::endl;

	std::cout << "width: " << width << std::endl;
	std::cout << "height: " << height << std::endl;

	const char* bg_file =
		meta_node.find_child_by_attribute("data", "name", "background")
			.attribute("value").value();

	// Load background asset
	m_asset_background = (sg::TextureAsset*)
		&(sg::GameAssetManager::instance().get_asset(bg_file));

	// Mapdata
	pugi::xml_node mapdata_node = doc.child("map").child("mapdata");
	if(!mapdata_node)
		std::cout << "Error, no mapdata" << std::endl;

	// Extract mapdata
	const char* mapdata = mapdata_node.child_value();
	int x = 0, y = height;

	b2BodyDef tile_def;
	b2Body *tile_body;
	b2PolygonShape tile_shapes[5];

	b2Vec2 tl_vec[3];
	b2Vec2 tr_vec[3];
	b2Vec2 bl_vec[3];
	b2Vec2 br_vec[3];

	// 
	// 1         1        1         1
	// |    #    | #       | ####    | ####
	// |   ##    | ##      |  ###    | ###
	// |  ###    | ###     |   ##    | ##
	// | ####    | ####    |    #    | #
	// 0-----1   0-----1   0-----1   0-----1
	//

	tl_vec[0].Set( 0.5f,  0.5f);
	tl_vec[1].Set(-0.5f, -0.5f);
	tl_vec[2].Set( 0.5f, -0.5f);
	tile_shapes[Tile::TOP_LEFT].Set(tl_vec, 3);

	tr_vec[0].Set(-0.5f,  0.5f);
	tr_vec[1].Set(-0.5f, -0.5f);
	tr_vec[2].Set( 0.5f, -0.5f);
	tile_shapes[Tile::TOP_RIGHT].Set(tr_vec, 3);

	bl_vec[0].Set(-0.5f,  0.5f);
	bl_vec[1].Set( 0.5f, -0.5f);
	bl_vec[2].Set( 0.5f,  0.5f);
	tile_shapes[Tile::BOTTOM_LEFT].Set(bl_vec, 3);

	br_vec[0].Set(-0.5f,  0.5f);
	br_vec[1].Set(-0.5f, -0.5f);
	br_vec[2].Set( 0.5f,  0.5f);
	tile_shapes[Tile::BOTTOM_RIGHT].Set(br_vec, 3);

	tile_shapes[Tile::BLOCK].SetAsBox(.5f, .5f);

	foreach(char ch, mapdata) {
		std::cout << ch;

		tile_def.position.Set(x, y);

		switch(ch) {
		case '#':
			m_tiles.push_back(new Tile(Tile::BLOCK, x, y));
			tile_body = m_world->CreateBody(&tile_def);
			tile_body->CreateFixture(&tile_shapes[Tile::BLOCK], 0.0f);
			break;
		case 'Q':
			m_tiles.push_back(new Tile(Tile::TOP_LEFT, x, y));
			tile_body = m_world->CreateBody(&tile_def);
			tile_body->CreateFixture(&tile_shapes[Tile::TOP_LEFT], 0.0f);
			break;
		case 'W':
			m_tiles.push_back(new Tile(Tile::TOP_RIGHT, x, y));
			tile_body = m_world->CreateBody(&tile_def);
			tile_body->CreateFixture(&tile_shapes[Tile::TOP_RIGHT], 0.0f);
			break;
		case 'A':
			m_tiles.push_back(new Tile(Tile::BOTTOM_LEFT, x, y));
			tile_body = m_world->CreateBody(&tile_def);
			tile_body->CreateFixture(&tile_shapes[Tile::BOTTOM_LEFT], 0.0f);
			break;
		case 'S':
			m_tiles.push_back(new Tile(Tile::BOTTOM_RIGHT, x, y));
			tile_body = m_world->CreateBody(&tile_def);
			tile_body->CreateFixture(&tile_shapes[Tile::BOTTOM_RIGHT], 0.0f);
			break;
		}

		if (ch == '\n'){
			y--;
			x = 0;
		} else {
			x++;
		}
	}


	// Create player
	sg::ID id = m_gm.entity_manager()->create_entity("ship");
	Ship& ship = (Ship&)m_gm.entity_manager()->get_entity(id);
	ship.set_controller(m_gm.player_controller());
	m_gm.player_controller()->set_controlled_entity(&ship);

	// Set camera to follow player
	((sg::EntityCameraMan*)m_gm.camera())->follow_entity(&ship);
	
	std::cout << std::endl;
}

// ----------------------------------------------------------------------------
void GameWorld::render_world() {
	// Draw walls
	glPushMatrix();

	glLineWidth(1.5f);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	
	// Level data
	foreach(Tile& tr, m_tiles) {
		glPushMatrix();
		tr.render();
		glPopMatrix();
	}
	glPopMatrix();
}

// ----------------------------------------------------------------------------
void GameWorld::pre_camerman_render() {
	glPushMatrix();
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, m_asset_background->texture());

	glBegin(GL_QUADS);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glTexCoord2i(0, 0);
		glVertex3f(0,  0, 0.0f);
		
		glTexCoord2i(1, 0);
		glVertex3f(m_width,  0, 0.0f);
		
		glTexCoord2i(1, 1);
		glVertex3f(m_width, m_height, 0.0f);
		
		glTexCoord2i(0, 1);
		glVertex3f(0, m_height, 0.0f);
	glEnd();

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

// ----------------------------------------------------------------------------
void GameWorld::do_logic(double timestep) {
	m_world->Step(timestep, 8, 3);
}

///////////////////////////////////////////////////////////////////////////////
//
// ContactListener
// ----------------------------------------------------------------------------
void ContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
	b2WorldManifold worldManifold;
	contact->GetWorldManifold(&worldManifold);
	b2PointState state1[2], state2[2];
	b2GetPointStates(state1, state2, oldManifold, contact->GetManifold());
	
	if (state2[0] == b2_addState) {
		const b2Body* body_a = contact->GetFixtureA()->GetBody();
		const b2Body* body_b = contact->GetFixtureB()->GetBody();
		b2Vec2 point = worldManifold.points[0];
		b2Vec2 v_a = body_a->GetLinearVelocityFromWorldPoint(point);
		b2Vec2 v_b = body_b->GetLinearVelocityFromWorldPoint(point);

		float32 approach_velocity = b2Dot(v_a - v_b, worldManifold.normal);

		sg::Entity* entity_a = static_cast<sg::Entity*>(body_a->GetUserData());
		sg::Entity* entity_b = static_cast<sg::Entity*>(body_b->GetUserData());

		if(entity_a) {
			sg::Entity::COLLISION_DATA cdata = {
				entity_b,
				approach_velocity
			};
			entity_a->send_message(sg::Entity::COLLISION,
				(sg::Entity::MSG_DATA*)&cdata);
		}

		if(entity_b) {
			sg::Entity::COLLISION_DATA cdata = {
				entity_a,
				approach_velocity
			};
			entity_b->send_message(sg::Entity::COLLISION,
				(sg::Entity::MSG_DATA*)&cdata);
		}
	}
}