#ifndef sgame_game_h
#define sgame_game_h

#include "entity.h"
#include "representation.h"
#include "view.h"
#include "gameworld.h"
#include "camera.h"

class ShipPlayerController;

namespace sg {

    ////////////////////////////////////////////////////////////////////////////
    // Summary:
    //    Manages all game aspects, there should only be one of this.
    class GameManager {
    public:
        GameManager();
        virtual ~GameManager();

        void initialize_game(); // Load all entity types

        EntityManager* entity_manager() {return &m_entityManager;}
        RepresentationManager* representation_manager() { return &m_representationManager; }
        View* view() { return &m_view; }

        GameWorld* gameworld() { return &m_gameworld; }
        CameraMan* camera() { return m_camera; }
        ShipPlayerController* player_controller() { return m_controller; }


    private:
        EntityManager m_entityManager;
        RepresentationManager m_representationManager;
        View m_view;
        GameWorld m_gameworld;
        ShipPlayerController* m_controller;

        CameraMan* m_camera;
    };
};

#endif