#ifndef sgame_camera_h
#define sgame_camera_h
#include "types.h"

namespace sg {

	class Entity;
	class View;

	// ------------------------------------------------------------------------
	//! Handles the camera
	/*!
	    Base class for all cameras.
	*/
	class CameraMan {
	public:
		CameraMan(View& view);
		virtual ~CameraMan();

		//! camera logic, move camera, calculations etc...
		virtual void do_logic() = 0;

		//! render camera
		virtual void render();

		//! scale up view
		virtual void zoomin(float amount=1.0);

		//! scale down view
		virtual void zoomout(float amount=1.0);

		//! get camera position
		Vector2f position();

		//! center view on world point
		void center_on(Vector2f pos);

		Vector2f current_center();

		//! get camera scale (zoom)
		float scale();

	protected:
		View& m_view;        //!< Reference to view
		Vector2f m_position; //!< camera position
		float m_scale;       //!< scale/zoom
	};


	// ------------------------------------------------------------------------
	//! Camera man that follows a specific entity
	class EntityCameraMan : public CameraMan {
	public:
		EntityCameraMan(View& view);
		virtual ~EntityCameraMan();

		//! Move camera towards entity
		void do_logic();

		//! Sett entity to follow
		void follow_entity(Entity* entity);

	private:
		Entity* m_entity;
		Vector2f m_old_position;
	};

	// ------------------------------------------------------------------------
	//! Manual handle of camera
	class ManualCameraMan : public CameraMan {
	public:
		ManualCameraMan(View& view);
		virtual ~ManualCameraMan();

		void do_logic();

		//! Move camera to the left
		void panleft(float amount=5.0);

		//! Move camera to the right
		void panright(float amount=5.0);

		//! Move camera up
		void panup(float amount=5.0);

		//! Move camera down
		void pandown(float amount=5.0);
	};

};

#endif // sgame_camera_h