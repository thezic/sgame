#ifndef types_h
#define types_h

#include <iostream>
#include "math.h"
#define PI 3.1415
namespace sg {
	
	typedef unsigned int ID;
	typedef unsigned long LPARAM;

	// Typedefs
	template<class T>
	class Vector2 {
	public:
		Vector2() : x(0), y(0) {}
		Vector2(T x, T y) : x(x), y(y) {}

		Vector2<T> operator/(T divider) const {
			Vector2<T> result(x/divider, y/divider);
			return result;
		}

		Vector2<T> operator*(T multiplier) const {
			Vector2<T> result(x * multiplier, y * multiplier);
			return result;
		}

		Vector2<T> operator+(const Vector2<T> &other) const {
			Vector2<T> result(x+other.x, y+other.y);
			return result;
		}

		Vector2<T> operator-(const Vector2<T> &other) const {
			Vector2<T> result(x-other.x, y-other.y);
			return result;
		}

		T abs() const {
			return (T)sqrt( (x*x) + (y*y));
		}

		std::ostream& operator<<(std::ostream& os) {
			return os << "(" << x << ", " << y << ")";
		}

		T x;
		T y;
	};

	template<class T>
	std::ostream& operator<<(std::ostream& stream, const Vector2<T>& v) {
		return stream << "(" << v.x << ", " << v.y << ")";
	}

	// template<class T>
	// std::ostream& operator<<(std::ostream&, const Vector2<T>&);

	typedef Vector2<float> Vector2f;
};

#endif