#ifndef __SGAME_EXCEPTION_H_
#define __SGAME_EXCEPTION_H_

#include <stdio.h>
#include <sstream>
#include "../pugixml/pugixml.hpp"
#include <SDL/SDL.h>

namespace sg {
	class Exception{
	public:
		Exception(const char *reason) : reason(reason) { }
		
		std::string reason;
	};

	class FileError : public Exception {
	public:
		FileError(const char *reason) : Exception(reason) { }
	};

	class XmlFileError : public FileError {
	public:
		XmlFileError(pugi::xml_parse_result result) : FileError("") {
			std::stringstream out;
			out << result.description() <<  ", at: " << result.offset;
			reason = out.str();
		}
	};


	class SDLException : public Exception {
	public:
		SDLException(const char *info) : Exception(""){

			reason = std::string(info) + std::string(": ") + SDL_GetError();
		}
	};
};
#endif
