#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <map>
namespace sg {

	//! Game confiuration class
	/*!
	    Collects settings
	*/
	class Config {
	public:
		Config();
		~Config();

		//! Fetch a property string
		/*!
		    \param property Name of property to fetch
		    \return saved property as a string
		*/
		std::string get_string(std::string property);

		//! Load configuration from xml config file
		/*!
		    If file does not exist, throw FileError
		    \param path path to config file
		    \param no_exception don't throw exception
		    \return true if successful, false if not
		*/
		bool load_config_file(const char* path, bool no_exception=false);

		//! Return instance of config singleton
		static Config& instance();

	private:
		std::map<std::string, std::string> m_properties;
	};
};

#endif // CONFIG_H