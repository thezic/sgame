#ifndef sgame_entity_h
#define sgame_entity_h

#include <map>
#include <string>
#include <memory>
#include <boost/function.hpp>
#include <boost/functional/factory.hpp>
#include <boost/ptr_container/ptr_map.hpp>
#include <boost/ptr_container/ptr_deque.hpp>
#include <boost/signals2.hpp>

#include "types.h"
#include "events.h"

#define ENTITY_BOILERPLATE(Klass, name) \
public: \
	Klass(sg::ID entity_id, sg::GameManager& gm, sg::LPARAM data) \
	: sg::Entity(entity_id, gm) { \
		initialize_entity(data); \
	} \
	virtual ~Klass() { destroy_entity(); } \
	static sg::IEntity* __stdcall create(sg::ID id, sg::GameManager& gm, sg::LPARAM data) { \
		return new Klass(id, gm, data); \
	} \
	std::string type_id() { return std::string(name); } \
	void initialize_entity(sg::LPARAM); \
	void destroy_entity();

namespace sg {

	class IEntity;
	class GameManager;

	typedef boost::function<IEntity* (ID, GameManager&, sg::LPARAM)> EntityFactory;
	typedef boost::ptr_map<ID, IEntity> EntityMap;
	typedef boost::signals2::signal<void (EntityEvent&)> entity_signal_t;

	class EntityManager {
	public:
		EntityManager(GameManager& gm);
		~EntityManager();
		//static EntityManager* instance(); // Return Singleton instance

		ID create_entity(std::string entity_id, sg::LPARAM data=NULL);
		IEntity& get_entity(ID entity_id);
		
		void register_entity(std::string id, EntityFactory factory);
		void do_logic();
		void do_loop_cleanup();

		boost::signals2::connection connect(const entity_signal_t::slot_type &subscriber);

	private:
		ID m_next_entity_id;
		std::map<std::string, EntityFactory> m_factories;
		EntityMap m_entities;
		GameManager& m_gm;

		entity_signal_t m_sig;
	};

	// -------------------------------------------------------------
	// Entity interface
	// -------------------------------------------------------------
	class IEntity {
	public:
		IEntity(void) {}
		virtual ~IEntity() {}

		void do_handle(void) {
			handle();
		}

		void do_loop_cleanup() {
			loop_cleanup();
		}

		virtual std::string type_id() = 0;

	private:
		virtual void handle(void) = 0;
		virtual void loop_cleanup() = 0;
	};

	class Entity : public IEntity{
	public:
		enum MSG_ID { COLLISION };
		struct COLLISION_DATA {
			Entity* other_entity;
			float impact_velocity;
		};
		union MSG_DATA {
			long l;
			COLLISION_DATA collision_data;
		};

		struct _MSG_DATA {
			MSG_ID id;
			MSG_DATA data;
		};
	public:
		Entity(ID id, GameManager& gm) : m_id(id), m_gm(gm) {}
		virtual ~Entity() {}

		void set_position(const Vector2f pos) {
			set_position(pos.x, pos.y);
		}
		virtual void set_position(float x, float y) {
			m_pos = Vector2f(x, y);
		}

		virtual Vector2f get_position() {
			return m_pos;
		};


		void send_message(MSG_ID id, MSG_DATA* msg_data) {
			_MSG_DATA *data = new _MSG_DATA;
			data->id = id;
			memcpy(&data->data, msg_data, sizeof(MSG_DATA));
			m_msg_queue.push_back(data);
		}

	private:
		virtual void loop_cleanup() { }

	protected:
		const ID m_id;
		Vector2f m_pos;
		GameManager& m_gm;

	protected:
		boost::ptr_deque<_MSG_DATA> m_msg_queue;
	};

};

#endif