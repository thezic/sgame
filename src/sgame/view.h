#ifndef sgame_view_h
#define sgame_view_h

#include "types.h"

namespace sg {

	// forward declerations
	class GameManager;
	class CameraMan;

	class View {
	public:
		View(int w, int h, GameManager& gm);
		virtual ~View();

		void render_view();

		void set_viewport();

		Vector2f get_wh();

		Vector2f convert_screen_cords(const Vector2f& pos);

	private:
		void do_view_logic();

	private:
		int m_w;
		int m_h;

		GameManager& m_gm;
		CameraMan* m_camera;
	};
};

#endif