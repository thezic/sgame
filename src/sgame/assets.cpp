#include "assets.h"
#include "config.h"
#include "exception.h"
#include "SDL_image/SDL_image.h"

using namespace sg;
/////////////////////////////////////////////////////////////////////////////
//
// GameAssetManager
//
/////////////////////////////////////////////////////////////////////////////

// ------ Construction / Destruction --------
GameAssetManager::GameAssetManager() : m_xml_is_loaded(false) {
	register_asset_type("texture", &(TextureAsset::create));
};

GameAssetManager::~GameAssetManager() {

}

// --------------------------------------------------------------------------
GameAssetManager& GameAssetManager::instance() {
	// quick and ugly singleton
	static GameAssetManager instance;
	return instance;
}

// --------------------------------------------------------------------------
void GameAssetManager::register_asset_type(std::string id, AssetFactory factory) {
	m_factories[id] = factory;
}

// --------------------------------------------------------------------------
IGameAsset& GameAssetManager::get_asset(std::string asset_name) {
	try {
		return m_assets.at(asset_name);
	} catch( boost::bad_ptr_container_operation& e ) {
		// asset not found, try to loading it first
		load_asset(asset_name);
		return m_assets.at(asset_name);
	} 
}

// --------------------------------------------------------------------------
void GameAssetManager::load_asset(std::string asset_name) {
	std::cout << "Loading asset: " <<  asset_name << std::endl;

	if (!m_xml_is_loaded) {
		this->load_file();
	}

	pugi::xpath_variable_set vars;
	vars.add("name", pugi::xpath_type_string);
	vars.set("name", asset_name.c_str());
	pugi::xpath_node node = m_doc.select_single_node(
		"/assets/asset[@name = string($name)]", &vars
	);

	if (node) {
		this->load_asset_from_node(node.node());
	}
}

// --------------------------------------------------------------------------
void GameAssetManager::load_file() {
	if (m_xml_is_loaded) {
		// File is already loaded, don't load it again return
		return;
	}

	const char* path = Config::instance().get_string("assets_definition_xml").c_str();
	pugi::xml_parse_result result = m_doc.load_file(path);

	m_xml_is_loaded = true;
	if (!result) {
		throw XmlFileError(result);
	}
}

// --------------------------------------------------------------------------
void GameAssetManager::load_asset_from_node(pugi::xml_node node) {
	std::string type = node.attribute("type").value();
	std::string name = node.attribute("name").value();

	try {
		IGameAsset* asset =  (m_factories[type])(node);
		m_assets.insert(name, asset);
	} catch (Exception e) {
		std::cout << "Error loading asset, " << e.reason << std::endl;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
// TextureAsset
//
/////////////////////////////////////////////////////////////////////////////
TextureAsset::TextureAsset(pugi::xml_node node) {
	std::cout << "Creating texture asset " << node.attribute("name").value() << ", ";

	const char* filename = node.find_child_by_attribute("data", "name", "image").
			attribute("value").value();

	std::cout << "image: " << filename << std::endl;
	//pugi::xpath_node fname_node = node.select_single_node("data@[name='image']");

	//const char* filename = fname_node.node().attribute("value").value();
	

	SDL_Surface *surface = IMG_Load(filename);
	if (!surface)
		throw SDLException("Unable to load image file");
	
	m_width = surface->w;
	m_height = surface->h;
	m_bpp = surface->format->BytesPerPixel;
	
	if (m_bpp == 4) {
		if (surface->format->Rmask == 0x000000ff)
			m_texture_format = GL_RGBA;
		else
			m_texture_format = GL_BGRA;
	}
	else if (m_bpp == 3) {
		if (surface->format->Rmask == 0x000000ff)
			m_texture_format = GL_RGB;
		else
			m_texture_format = GL_BGR;
	}
	else
		throw Exception("image not in truecolor");
		
	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	glTexImage2D(GL_TEXTURE_2D, 0, m_bpp, m_width, m_height, 0,
		m_texture_format, GL_UNSIGNED_BYTE, surface->pixels);
	
	SDL_FreeSurface(surface);

}

// --------------------------------------------------------------------------
TextureAsset::~TextureAsset() {
	glDeleteTextures(1, &m_texture);
}

// --------------------------------------------------------------------------
IGameAsset* __stdcall TextureAsset::create(pugi::xml_node node) { 
	return new TextureAsset(node);
}

// --------------------------------------------------------------------------
GLuint TextureAsset::texture() {
	return m_texture;
}

// --------------------------------------------------------------------------
float TextureAsset::aspect_ratio() {
	return (float)m_width / (float)m_height;
}