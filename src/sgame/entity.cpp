#include <iostream>
#include "entity.h"

using namespace sg;

//EntityManager* EntityManager::s_instance = 0;

// Construction / Desctruction
EntityManager::EntityManager(GameManager& gm) : m_next_entity_id(0), m_gm(gm) {

}

EntityManager::~EntityManager() {
	std::cout << "Destroying EntityManager" << std::endl;
}

////////////////////////////////////////////////////////////////////////////////
// Summary:
//     returns singleton instance
// EntityManager* EntityManager::instance() {
// 	static EntityManager instance;
// 	return &instance;
// }

////////////////////////////////////////////////////////////////////////////////
// Summary:
//     Create a new entity
ID EntityManager::create_entity(std::string entity_type_id, LPARAM data) {
	ID entity_id = m_next_entity_id++;

	IEntity* entity = (m_factories[entity_type_id])(entity_id, m_gm, data);

	
	m_entities.insert(entity_id, entity);
	std::cout << "created a " << entity_type_id << " with id " << entity_id << std::endl;

	// Emit event
	EntityEvent event(ENTITY_CREATED, entity_id);
	m_sig(event);

	return entity_id;
}

////////////////////////////////////////////////////////////////////////////////
// Summary:
//     Register a new entity factory
//
// Parameters:
//     `id`: std::string, type id of entity
//     `factory`: EntityFactory, function pointer to factory function
void EntityManager::register_entity(std::string id, EntityFactory factory) {
	m_factories[id] = factory;
}

////////////////////////////////////////////////////////////////////////////////
// Summary:
//     connect a subscriber to the signal
//
// Parameters:
//     `subscriber`: function pointer to subscriber
//
// return value:
//     returns a boost::signals2::connection, containing connection
boost::signals2::connection EntityManager::connect(const entity_signal_t::slot_type &subscriber) {
	return m_sig.connect(subscriber);
}

////////////////////////////////////////////////////////////////////////////////
// Summary:
//     returns entity with specific id
//
// Paramerers:
//     `entity_id`: ID, id of entity to be returned
//
// Return type:
//     Reference to entity
IEntity& EntityManager::get_entity(ID entity_id) {
	IEntity& entity = m_entities.at(entity_id);
	return entity;
}

////////////////////////////////////////////////////////////////////////////////
// Summary:
//     handles logic for entities store.
void EntityManager::do_logic() {
	for(EntityMap::iterator i=m_entities.begin(), e=m_entities.end(); i != e; i++) {
		i->second->do_handle();
	}
}

// ----------------------------------------------------------------------------
void EntityManager::do_loop_cleanup() {
	for(EntityMap::iterator i=m_entities.begin(), e=m_entities.end(); i != e; i++) {
		i->second->do_loop_cleanup();
	}
}