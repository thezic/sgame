#include "game.h"
#include "gameworld.h"
#include "../ball.h"
#include "../ship.h"
#include "../weapons.h"

#include <iostream>
using namespace sg;

#define REGISTER_ENTITY(id, ent, repr) \
	m_entityManager.register_entity(id, &(ent::create)); \
	m_representationManager.register_representation(id, &(repr::create));

GameManager::GameManager() :
		m_view(800, 600, *this),
		m_gameworld(*this),
		m_entityManager(*this),
		m_representationManager(m_entityManager) {

	// Load entity types
	REGISTER_ENTITY("ball", Ball, BallRepresentation);
	REGISTER_ENTITY("ship", Ship, ShipRepresentation);
	REGISTER_ENTITY("bullet", Bullet, BulletRepresentation);

	m_camera = new EntityCameraMan(m_view);
	m_controller = new ShipPlayerController(m_view);
}
GameManager::~GameManager() {
	std::cout << "Destroy GameManager" << std::endl;
	delete m_camera;
	delete m_controller;
}

////////////////////////////////////////////////////////////////////////////////
// Summary:
//    Initialize game state
void GameManager::initialize_game() {
	std::cout << "Initialize game (core)" << std::endl;
    m_gameworld.load_level("spacewar");
}
