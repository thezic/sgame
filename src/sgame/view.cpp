
#include "view.h"
#include "game.h"
#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include "representation.h"
#include "camera.h"

using namespace sg;

// ----------------------------------------------------------------------------
View::View(int w, int h, GameManager& gm) :
		m_w(w), m_h(h), m_gm(gm) {

}

// ----------------------------------------------------------------------------
View::~View() {
	std::cout << "Destroy view " << std::endl;
}

// ----------------------------------------------------------------------------
void View::render_view() {
	do_view_logic();

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	glPushMatrix();
	glTranslatef(
		m_gm.camera()->position().x/2 - 200,
		m_gm.camera()->position().y/2 - 200,
		0.0f);
	glScalef(30, 30, 0);
	m_gm.gameworld()->pre_camerman_render();
	glPopMatrix();

	// Camera man render stuff
	m_gm.camera()->render();

	// Draw world
	m_gm.gameworld()->render_world();

	//render entities
	m_gm.representation_manager()->do_render();
	
	SDL_GL_SwapBuffers();
}

// ----------------------------------------------------------------------------
void View::set_viewport() {
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glViewport(0, 0, m_w, m_h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, m_w, 0, m_h, 1, -1);
}

// ----------------------------------------------------------------------------
Vector2f View::convert_screen_cords(const Vector2f& pos) {
	Vector2f mpos(pos.x, m_h - pos.y);
	Vector2f wpos = mpos - m_gm.camera()->position();
	wpos = wpos/m_gm.camera()->scale();

	wpos << std::cout << std::endl;
	//std::cout << wpos << std::endl;

	return wpos;

}

// ----------------------------------------------------------------------------
void View::do_view_logic() {

}

// ----------------------------------------------------------------------------
Vector2f View::get_wh() {
	return Vector2f(m_w, m_h);
}
