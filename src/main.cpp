#include <iostream>
#include "app.h"
#include "sgame/exception.h"
#include "sgame/config.h"

int main(int argc, char **argv) {

	// Load config
	try {
        sg::Config::instance().load_config_file("config.xml");
    } catch (sg::FileError e) {
        std::cout << "Unable to load config: " << e.reason << std::endl;
        return EXIT_FAILURE;
    }

	CApp app(argc, argv);
	return app.run();
}
