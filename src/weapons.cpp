#include "weapons.h"
#include "sgame/game.h"

///////////////////////////////////////////////////////////////////////////////
//
// Bullet Entity
// ----------------------------------------------------------------------------
void Bullet::initialize_entity(sg::LPARAM data) {
	INIT_STRUCT* ldata = (INIT_STRUCT*)data;
	m_parent = ldata->parent;
	m_aim = ldata->aim;

	sg::Vector2f ppos = m_parent->get_position();

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(ppos.x, ppos.y);
	bodyDef.gravityScale = 0.0f;
	bodyDef.bullet = true;

	m_body = m_gm.gameworld()->world()->CreateBody(&bodyDef);
	b2CircleShape shape;
	shape.m_radius = 0.1;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.1f;

	// Collide with everything except ship
	fixtureDef.filter.categoryBits = 0x0002;
	fixtureDef.filter.maskBits = 0x0001;

	sg::Vector2f impulse = m_aim - m_parent->get_position();
	impulse = impulse * 500 / impulse.abs();
	m_body->SetUserData(this);
	m_body->CreateFixture(&fixtureDef);
	m_body->ApplyForce(
			b2Vec2(impulse.x, impulse.y),
			m_body->GetWorldCenter());

	std::cout << "bullet " << get_position() << std::endl;
	m_alive = true;
}

// ----------------------------------------------------------------------------
sg::Vector2f Bullet::get_position() {
	b2Vec2 pos = m_body->GetPosition();
	return sg::Vector2f(pos.x, pos.y);
}

// ----------------------------------------------------------------------------
void Bullet::destroy_entity() {
	
}

// ----------------------------------------------------------------------------
void Bullet::handle() {
	while(!m_msg_queue.empty()) {
		sg::Entity::_MSG_DATA& msg = m_msg_queue.front();
		switch(msg.id) {
		case sg::Entity::COLLISION:
			std::cout << "Bullet collide" << std::endl;
			m_alive = false;
			break;
		};
		m_msg_queue.pop_front();
	}
}

// ----------------------------------------------------------------------------
void Bullet::loop_cleanup() {
	if(m_body && !m_alive) {
		m_body->GetWorld()->DestroyBody(m_body);
		m_body = NULL;
	}
}


///////////////////////////////////////////////////////////////////////////////
//
// Bullet Representation
// ----------------------------------------------------------------------------
void BulletRepresentation::initialize_entity() {

}
// ----------------------------------------------------------------------------
void BulletRepresentation::destroy_entity() {

}
// ----------------------------------------------------------------------------
void BulletRepresentation::render() {

	if (!m_entity.m_alive) {
		return;
	}

	float radius = 0.1;
	sg::Vector2f pos = m_entity.get_position();
	
	glPushMatrix();

	glLineWidth(1.5f);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glTranslatef(pos.x, pos.y, 0.0f);
	glScalef(radius, radius, 1.0f);

	
	glBegin(GL_POLYGON);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		for(float fi=2*PI/15; fi<2*PI; fi+=2*PI/15) {
			glVertex3f(cos(fi), sin(fi), 0.0f);
		}
	glEnd();
	glPopMatrix();
}