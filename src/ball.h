#ifndef ball_h
#define ball_h

#include <Box2D/Box2D.h>
#include "sgame/sgame.h"

#define PI 3.1415

namespace sg {
	class GameManager;
}

class Ball: public sg::Entity {
	ENTITY_BOILERPLATE(Ball, "ball")
public:
	float get_radius() const;

	sg::Vector2f get_position();

	void set_position(sg::Vector2f newPos);

	void set_angle(float fi);

	float get_angle();

private:
	virtual void handle();

	float m_radius;

	b2Body *m_body;
};


class BallRepresentation: public sg::IEntityRepresentation {
public:
	BallRepresentation(Ball& ball_entity);
	virtual ~BallRepresentation();

	static sg::IEntityRepresentation* __stdcall create(sg::IEntity& entity);

	virtual void render();

private:
	Ball& m_ball_entity;
};

#endif