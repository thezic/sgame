#include <iostream>
#include <boost/bind.hpp>

#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include "app.h"

#include "sgame/exception.h"
#include "sgame/sgame.h"
#include "sgame/types.h"
#include "ship.h"

#include <Box2D/Box2D.h>

CApp::CApp(int argc, char **argv) {
    std::cout << "Creating application" << std::endl;
    screen = NULL;
    running = false;
    memset(m_pan, 0, sizeof(m_pan));
    m_timestep  = 1.0/60.0; // 60Hz
}

CApp::~CApp() {
    std::cout << "Destroy application" << std::endl;
}

void CApp::init(){
    std::cout << "Initialize application" << std::endl;
    
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        throw sg::SDLException("Unable to initialize SDL");
    
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,        1);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,            8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,          8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,           8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,          8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,         16);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,        32);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE,      8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE,    8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE,     8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE,    8);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,  1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,  2);
    
    screen = SDL_SetVideoMode(800, 600, 16, SDL_HWSURFACE|SDL_OPENGL);  
    m_gm.view()->set_viewport();
    
    glMatrixMode(GL_MODELVIEW);
    //glEnable(GL_TEXTURE_2D);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    //glEnable(GL_BLEND);
    //glDisable(GL_DEPTH_TEST);
    glLoadIdentity();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    m_gm.initialize_game();
    
    running = true;
}

void CApp::event_handler() {
    SDL_Event event;

    while(SDL_PollEvent(&event)){
        switch(event.type) {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT) {
                    std::cout << "click (" << event.button.x << ", " << event.button.y << ")\n";

                    sg::Vector2f pos = m_gm.view()->convert_screen_cords(sg::Vector2f(event.button.x, event.button.y));

                    sg::ID id = m_gm.entity_manager()->create_entity("ball");
                    Ship& ship = (Ship&)m_gm.entity_manager()->get_entity(id);
                    ship.set_position(pos);

                } else if(event.button.button == SDL_BUTTON_WHEELUP) {
                    m_gm.camera()->zoomin();

                } else if (event.button.button == SDL_BUTTON_WHEELDOWN) {
                    m_gm.camera()->zoomout();
                }
                break;

            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE)
                    running = false;
                break;

            case SDL_KEYUP:
                break;
        }
    }
}

int CApp::run(){
    try {
        init();
    }
    catch(sg::Exception e) {
        std::cout << "ERROR: " << e.reason << std::endl;
        return EXIT_FAILURE;
    }

    double next_time = SDL_GetTicks()/1000.0; // Convert to seconds
    double last_render_time;
    int skipped_frames = 0;

    // The game loop
    while(running){
        Uint32 start=SDL_GetTicks();
        double current_time = SDL_GetTicks()/1000.0;

        if (current_time >= next_time) {
            next_time += m_timestep;

            event_handler();
            do_logic();

            if (current_time < next_time){
                 do_render();
                 //std::cout << "fps: " << (int)(1.0 / (current_time - last_render_time)) << " (" << skipped_frames << ")" << std::endl;
                 last_render_time = current_time;
                 skipped_frames = 0;
             } else {
                skipped_frames++;
             }
        } else {
            Uint32 sleep_time = (Uint32)1000.0*(next_time - current_time);
            if (sleep_time > 0)
                SDL_Delay(sleep_time);
        }
    }
    
    
    SDL_Quit();
    return EXIT_SUCCESS;
}

void CApp::do_logic() {
    // step physics simulation
    m_gm.gameworld()->do_logic(m_timestep);
    m_gm.entity_manager()->do_logic();
    m_gm.camera()->do_logic();

    m_gm.entity_manager()->do_loop_cleanup();
}

void CApp::do_render(){
    m_gm.view()->render_view();
}
