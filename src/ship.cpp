#include <string>
#include <iostream>
#include <math.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL.h>
#include "ship.h"
#include "sgame/view.h"
#include "sgame/game.h"
#include "weapons.h"

#define PI 3.1415

// ----------------------------------------------------------------------------
void Ship::initialize_entity(sg::LPARAM data) {
	m_controller = NULL;
	m_size = 1.0f;
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(10.0, 20.0);
	bodyDef.gravityScale = 0.0f;
	bodyDef.fixedRotation = true;
	bodyDef.linearDamping = 5.0f;

	m_body = m_gm.gameworld()->world()->CreateBody(&bodyDef);
	b2PolygonShape shipBox;
	shipBox.SetAsBox(m_size, m_size/2);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shipBox;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.1f;

	fixtureDef.filter.categoryBits = 0x0001;
	fixtureDef.filter.maskBits = 0x0001;

	m_body->CreateFixture(&fixtureDef);
}

// ----------------------------------------------------------------------------
void Ship::destroy_entity() {

}
// ----------------------------------------------------------------------------
float Ship::get_size() const { return m_size; }

// ----------------------------------------------------------------------------
sg::Vector2f Ship::get_position() {
	b2Vec2 pos = m_body->GetPosition();
	return sg::Vector2f(pos.x, pos.y);
}

// ----------------------------------------------------------------------------
void Ship::set_position(sg::Vector2f newPos) {
	b2Vec2 pos(newPos.x, newPos.y);
	m_body->SetTransform(pos, m_body->GetAngle());
}

// ----------------------------------------------------------------------------
void Ship::set_angle(float fi) {
	//m_body->SetAngle(fi);
}

// ----------------------------------------------------------------------------
float Ship::get_angle() {
	return m_body->GetAngle();
}

// ----------------------------------------------------------------------------
void Ship::set_controller(ShipController* controller) {
	m_controller = controller;
}

// ----------------------------------------------------------------------------
void Ship::handle() {
	//m_body->ApplyForce(b2Vec2(1, 0), m_body->GetWorldCenter());
	//m_body->ApplyAngularImpulse(1.0f);

	if(m_controller) {
		sg::Vector2f vec = m_controller->get_desired_thrust_vector();
		//m_body->ApplyLinearImpulse(
		m_body->ApplyForce(
			b2Vec2(vec.x, vec.y),
			m_body->GetWorldCenter());

		//std::cout << "vector: " << vec << std::endl;

		if(m_controller->is_pulling_trigger()) {
			std::cout << "aim at " << m_controller->get_desired_aim() << std::endl;
			

			Bullet::INIT_STRUCT initial = {
				this, m_controller->get_desired_aim()
			};
			sg::ID id = m_gm.entity_manager()->create_entity(
				"bullet", (sg::LPARAM)&initial);
			Bullet& bullet =
				static_cast<Bullet&>(m_gm.entity_manager()->get_entity(id));
		}
	}
}



////////////////////////////////////////////////////////////////////////////////
// Ship Representation functions
////////////////////////////////////////////////////////////////////////////////
ShipRepresentation::ShipRepresentation(Ship& ship_entity) : m_entity(ship_entity){
	m_asset_texture = (sg::TextureAsset*)&(sg::GameAssetManager::instance().get_asset("ship"));
}

// ----------------------------------------------------------------------------
ShipRepresentation::~ShipRepresentation() { }

// ----------------------------------------------------------------------------
sg::IEntityRepresentation* __stdcall ShipRepresentation::create(sg::IEntity& entity){
	Ship &ship_entity = *((Ship*)(&entity));
	return new ShipRepresentation(ship_entity);
}

// ----------------------------------------------------------------------------
void ShipRepresentation::render() {

	float size = m_entity.get_size();
	sg::Vector2f pos = m_entity.get_position();
	float aspect = m_asset_texture->aspect_ratio();
	float angle = m_entity.get_angle() * 180 / PI;
	
	glPushMatrix();
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	
	glBindTexture(GL_TEXTURE_2D, m_asset_texture->texture());

	glTranslatef(pos.x, pos.y, 0.0f);
	glRotatef(angle, 0, 0, 1);
	glScalef(size, size, 1.0f);

	
	glBegin(GL_QUADS);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glTexCoord2i(0, 0);
		glVertex3f(-aspect/2,  0.5f, 0.0f);
		
		glTexCoord2i(1, 0);
		glVertex3f( aspect/2,  0.5f, 0.0f);
		
		glTexCoord2i(1, 1);
		glVertex3f( aspect/2, -0.5f, 0.0f);
		
		glTexCoord2i(0, 1);
		glVertex3f(-aspect/2, -0.5f, 0.0f);
	glEnd();

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

///////////////////////////////////////////////////////////////////////////////
//
// Ship Control functions
//
///////////////////////////////////////////////////////////////////////////////
ShipPlayerController::ShipPlayerController(sg::View& view)
: ShipController(view) {
	m_scalar = 100;
	memset(m_thrust, 0, sizeof(m_thrust));
}

// ----------------------------------------------------------------------------
ShipPlayerController::~ShipPlayerController() {

}

// ----------------------------------------------------------------------------
#define BIND_THRUST_KEY(key, dir) m_thrust[dir] = (state[key] ? m_scalar : 0)
sg::Vector2f ShipPlayerController::get_desired_thrust_vector() {
	Uint8 *state = SDL_GetKeyState(NULL);

	BIND_THRUST_KEY(SDLK_w, up);
	BIND_THRUST_KEY(SDLK_s, down);
	BIND_THRUST_KEY(SDLK_a, left);
	BIND_THRUST_KEY(SDLK_d, right);
	
	return sg::Vector2f(
		m_thrust[right] - m_thrust[left],
		m_thrust[up] - m_thrust[down]);
}

// ----------------------------------------------------------------------------
sg::Vector2f ShipPlayerController::get_desired_aim() {
	int mouse_x, mouse_y;
	SDL_GetMouseState(&mouse_x, &mouse_y);
	sg::Vector2f mouse_world_position = 
		m_view.convert_screen_cords(sg::Vector2f(mouse_x, mouse_y));

	return mouse_world_position;
}

// ----------------------------------------------------------------------------
bool ShipPlayerController::is_pulling_trigger() {
	return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(1);
}
