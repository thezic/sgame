#include "sgame/sgame.h"

#include <string>
#include <iostream>
#include <math.h>
#include <SDL/SDL_opengl.h>
#include "ball.h"
#include "Box2D/Box2D.h"
#include "sgame/game.h"

#define PI 3.1415

void Ball::initialize_entity(sg::LPARAM data) {
	m_radius = 0.5f;
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(10.0, 20.0);

	m_body = m_gm.gameworld()->world()->CreateBody(&bodyDef);
	b2CircleShape ballBox;
	ballBox.m_radius = m_radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &ballBox;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.9f;

	m_body->CreateFixture(&fixtureDef);
}

void Ball::destroy_entity() {

}

float Ball::get_radius() const { return m_radius; }

sg::Vector2f Ball::get_position() {
	b2Vec2 pos = m_body->GetPosition();
	return sg::Vector2f(pos.x, pos.y);
}

void Ball::set_position(sg::Vector2f newPos) {
	b2Vec2 pos(newPos.x, newPos.y);
	m_body->SetTransform(pos, m_body->GetAngle());
}

void Ball::set_angle(float fi) {
	//m_body->SetAngle(fi);
}

float Ball::get_angle() {
	return m_body->GetAngle();
}

void Ball::handle() {

}



////////////////////////////////////////////////////////////////////////////////
// Ball Representation functions
////////////////////////////////////////////////////////////////////////////////
BallRepresentation::BallRepresentation(Ball& ball_entity) : m_ball_entity(ball_entity){ }
BallRepresentation::~BallRepresentation() { }

sg::IEntityRepresentation* __stdcall BallRepresentation::create(sg::IEntity& entity){
	Ball &ball_entity = *((Ball*)(&entity));
	return new BallRepresentation(ball_entity);
}

void BallRepresentation::render() {

	float radius = m_ball_entity.get_radius();
	sg::Vector2f pos = m_ball_entity.get_position();
	
	glPushMatrix();

	glLineWidth(1.5f);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glTranslatef(pos.x, pos.y, 0.0f);
	glScalef(radius, radius, 1.0f);

	
	glBegin(GL_POLYGON);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		for(float fi=2*PI/15; fi<2*PI; fi+=2*PI/15) {
			glVertex3f(cos(fi), sin(fi), 0.0f);
		}
	glEnd();
	glPopMatrix();
}