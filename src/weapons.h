#ifndef weapons_h
#define weapons_h

#include <Box2D/Box2D.h>
#include "sgame/sgame.h"

class Bullet : public sg::Entity {
	ENTITY_BOILERPLATE(Bullet, "bullet")
	friend class BulletRepresentation;

	struct INIT_STRUCT {
		sg::Entity* parent;
		sg::Vector2f aim;
	};

public:
	void handle();
	sg::Vector2f get_position();

private:
	virtual void loop_cleanup();

private:
	sg::Entity* m_parent;
	sg::Vector2f m_aim;
	b2Body* m_body;

	bool m_alive;
};

class BulletRepresentation : public sg::IEntityRepresentation {
	ENTITY_REPRESENTATION_BOILERPLATE(BulletRepresentation, Bullet)
public:

	void initialize_entity();
	void destroy_entity();
	void render();
};
#endif // weapons_h