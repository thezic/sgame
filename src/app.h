#ifndef __APP_H__
#define __APP_H__

#include <SDL/SDL.h>
//#include "sgame/view.h"
#include "sgame/game.h"

class CApp {
public:
	CApp(int argc, char **argv);
	~CApp();
	int run();

	void event_handler();
	
private:
	void init(void);
	void do_logic();
	void do_render();
	
	bool running;
	SDL_Surface *screen;

	//sg::View view;
	sg::GameManager m_gm;

	enum DIRECTION {LEFT, RIGHT, UP, DOWN};
	bool m_pan[4];
	double m_timestep;
};
#endif
