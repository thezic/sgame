import json
import sys

def main():
    filename = sys.argv[1]

    with open(filename) as f:
        obj = json.load(f)
        print obj

if __name__ == '__main__':
    main()