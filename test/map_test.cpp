
#include <iostream>

namespace ts {

	template<class T>
	class Test {
	public:
		Test() : m_var(10) {}

		template<class T2>
		friend std::ostream& operator<< (std::ostream&, const Test<T2>&);

	private:
		T m_var;
	};

	template<class T>
	std::ostream& operator<<(std::ostream& stream, const Test<T>& test) {
		return stream << test.m_var;
	}

	typedef Test<int> TestI;


	class Test2 {
	public:
		template<typename T>
		T& fun() {
			T var;
			return var;
		}
	};
};

int main(void) {

	ts::TestI test;

	std::cout << "Hello! "<< test << " \n";

	return 0;
}